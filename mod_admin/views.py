from flask import session,render_template,request,abort
from . import admin
from mod_users.forms import LoginForm
from mod_users.models import User

@admin.route('/')
def Index():
    return "Hello form admin Index"


@admin.route('/login/',methods=['GET' , 'POST'])
def login():
    #session['name'] = 'mamad'
    #session.clear()
    #print(session.get('name'))
    #print(session)
    form=LoginForm(request.form)
    if request.method == 'POST':
        if not form.validate_on_submit():
            abort(400) #when come to this line exit from view and return 400 error
        user = User.query.filter(User.email.ilike(f'{form.email.data}')).first()
        if not user:
            return "incorrect credentials",400
        if not user.check_password(form.password.data):
            return "incorrect credentials",400
        session['email'] = user.email
        session['user_id'] = user.id
        return "logged in successfully"
    if session.get('email') is not None:
        return "you are already logged in"

    return render_template('admin/login.html',form=form)
